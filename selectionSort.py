def findMin(arr):
    smallest = arr[0]
    smallest_index = 0
    for i in range(1, len(arr)):
        if arr[i] < smallest:
            smallest = arr[i]
            smallest_index = i
    return smallest_index


def selectionSort(arr):
    newArr = []
    for i in range(len(arr)):
        smallest = findMin(arr)
        newArr.append(arr.pop(smallest))
    return newArr

print(selectionSort([10,5,2,3,5,72,1476,72,4,1,5,8]))
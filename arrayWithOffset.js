const search = (arr, low = 0, high = arr.length - 1) =>{
    if (high < low)  return 0
    if (high == low) return low
    let mid = Math.floor(low + (high - low)/2)
   
    if (mid < high && arr[mid+1] < arr[mid]) {
       return mid+1 
    }
  
    if (mid > low && arr[mid] < arr[mid - 1]){
       return mid
    }
  
    if (arr[high] >= arr[mid]){
       return search(arr, low, mid-1) 
    }
    return search(arr, mid+1, high) 
}

console.log(search([3,4,5,1,2]))
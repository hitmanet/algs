class StackFromTwoQueue{
    constructor(){
        this.queue1 = []
        this.queue2 = []
    }

    add(el){
        this.queue1.push(el)
    }

    pop(){
        this._reverse()
        this.queue2.pop()
        console.log(this.queue2)
    }

    peek(){
        this._reverse()
        return this.queue2[this.queue2.length-1]
    }

    static isEmpty(queue){
        return queue.length === 0
    }

    _reverse(){
        if (!StackFromTwoQueue.isEmpty(this.queue2)){
            return
        }
        while(!StackFromTwoQueue.isEmpty(this.queue1)){
            this.queue2.push(this.queue1.shift())
        }
    }
}


window.onload = () =>{
    const stack = new StackFromTwoQueue()
    const arr = []
    stack.add(7)
    console.log(stack)
}
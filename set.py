def set_max():
    print('gui')
    states_need = set(["mt", "wa", "or", "id", "nv", "ut", "ca", "az"])
    stations = {
        "kone": set(["id", "nv", "ut"]),
        "ktwo": set(["wa", "id", "mt"]),
        "kthree": set(["or", "nv", "ca"]),
        "kfour": set(["nv", "ut"]),
        "kfive": set(["ca", "az"])
    }
    final_stations = set()
    while states_need:
        best_station = None
        states_covered = set()
        for station, states_for_station in stations.items():
            covered = states_need & states_for_station
            if len(covered) > len(states_covered):
                best_station = station
                states_covered = covered
    states_need -= states_covered
    final_stations.add(best_station)
    return final_stations


print(set_max())
import random

def quickSort(nums):
   if len(nums) <= 1:
       return nums
   else:
       q = random.choice(nums)
   l_nums = [n for n in nums if n < q]
   e_nums = [q] * nums.count(q)
   b_nums = [n for n in nums if n > q]
   return quickSort(l_nums) + e_nums + quickSort(b_nums)

print(quickSort([10,5,2,3,5,72,1476,72,4,1,5,8]))